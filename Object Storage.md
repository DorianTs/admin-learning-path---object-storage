* Be able to explain file storage, pros and cons and relevance in a world of
rapid modernization
* Explain block storage, including the use cases today
* What is object storage?
* How object storage works?
* What are the use cases for object?
* Benefits of using object
* Be familiar about the open source solutions today for object storage (Ceph, Swift, MinIO)
